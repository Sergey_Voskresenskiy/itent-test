const menuCloseBtn = document.querySelector('.header__menu__close-btn')
const menuOpenBtn = document.querySelector('.header__menu-btn')
const menuFade = document.querySelector('.header__fade')
const menuMenu = document.querySelector('.header__menu')

if (menuCloseBtn) {
  menuCloseBtn.addEventListener('click', e => {
    menuFade.style.opacity = 0
    menuMenu.style.transform = "translateX(1000px)"
    setTimeout(() => {
      menuFade.style.display = 'none'
    }, 1000)
  });
}
if (menuOpenBtn) {
  menuOpenBtn.addEventListener('click', e => {
    e.preventDefault()
    menuFade.style.opacity = 1
    menuFade.style.display = 'block'
    menuMenu.style.transform = "translateX(0px)"
  })
}
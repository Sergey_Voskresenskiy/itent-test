(() => {
  function requireAll(r) {
    r.keys().forEach(r);
  }
  requireAll(require.context("../images/sprites/", true, /\.svg$/));
  lazyload();

  $('.owl-carousel').owlCarousel({
    items: 3,
    margin:0,
    autoWidth:true,
    mouseDrag: false
  })

})();
